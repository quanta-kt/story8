from django.test import TestCase,Client
from django.urls import resolve
from .views import index

class Story7UnitTest(TestCase):

	def test_story_8_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_story_6_using_story_6_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'Home/Home.html')

	def test_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_story_6_landing_page(self):
		response = Client().get('/')
		response_content = response.content.decode('utf8')
		self.assertIn("Profile", response_content)